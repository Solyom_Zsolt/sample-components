import React, { Component, PropTypes } from 'react';

import FixedDataTable from 'fixed-data-table';

import withContext from '../../decorators/withContext';
import withStyles from '../../decorators/withStyles';

import StatefullEditableInput from '../statefull-editable-input/statefull-editable-input';

var Table = FixedDataTable.Table;
var Column = FixedDataTable.Column;

class TableComponent extends Component {

    constructor(props) {
        "use strict";
        super(props);
        this._renderCheckbox = this._renderCheckbox.bind(this);
        this._renderEditableInput = this._renderEditableInput.bind(this);
    }

    _renderCheckbox(data, rowKey, rowData) {
        "use strict";
        return <input type="checkbox" value={data} onChange={this.props.onSelectionChange.bind(this, rowData.get("queue_name"))} checked={data} />;
    }

    _cellDataGetter(cellKey, rowData) {
        "use strict";
        return rowData.get(cellKey);
    }

    _renderEditableInput(data, rowKey, rowData) {
        return (
            <StatefullEditableInput data={data}
                        rowKey={rowKey}
                        rowData={rowData}
                        onSetRowLabel={this.props.onSetRowLabel}/>
        );
    }

    render() {
        "use strict";
        return (

            <div className="table__container">

              <Table
                  height={300}
                  className="data-table"
                  width={1110}
                  rowsCount={this.props.tableData.size}
                  rowHeight={50}
                  headerHeight={50}
                  rowGetter={(rowIndex) => {
                      return this.props.tableData.get(rowIndex);
                  }}>

                  <Column
                      dataKey="id"
                      width={100}
                      label="ID"
                      cellDataGetter={this._cellDataGetter}/>
                  <Column
                      dataKey="queue_name"
                      width={340}
                      label="Queue Name"
                      cellDataGetter={this._cellDataGetter}/>
                  <Column
                      dataKey="service"
                      width={150}
                      label="Service"
                      cellDataGetter={this._cellDataGetter}/>
                  <Column
                      dataKey="opco"
                      width={100}
                      label="OPCO"
                      cellDataGetter={this._cellDataGetter}/>
                  <Column
                      dataKey="label"
                      width={350}
                      label="Label"
                      cellDataGetter={this._cellDataGetter}
                      cellRenderer={this._renderEditableInput}/>
                  <Column
                      dataKey="selected"
                      width={75}
                      label="Add to diagram"
                      cellDataGetter={this._cellDataGetter}
                      cellRenderer={this._renderCheckbox}/>
              </Table>

        </div>
      );
  }

}

export default TableComponent;
