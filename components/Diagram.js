/*! React Starter Kit | MIT License | http://www.reactstarterkit.com/ */

import React, { PropTypes, Component } from 'react';
import Immutable from 'immutable';

import plantumlEncoder from 'plantuml-encoder';
import Tabs from 'react-bootstrap/lib/Tabs';
import Tab from 'react-bootstrap/lib/Tab';
import TextEditor from 'react-texteditor';
import ReactQuill from 'react-quill';

class Diagram extends Component {

    constructor(props) {
        "use strict";
        super(props);
        this.state = {
            'selectedTab': 1
        }
        this.handleSelect = this.handleSelect.bind(this);
    }

    handleSelect(selectedTab) {
        "use strict";
        this.setState({
            'selectedTab': selectedTab
        });
    }

    _getUMLDiagramDataString() {
        "use strict";
        const selectedRows = this.props.tableData.filter(tableRow => tableRow.get("selected") === true);
        let textForUMLDiagram = "";
        selectedRows.map((tableRowData, index) => {
            switch ( this.state.selectedTab ) {
                case 1 :
                    if (index === 0) {
                        textForUMLDiagram += "[" + tableRowData.get("label") + "]"
                    } else {
                        textForUMLDiagram += " --> " + "[" + tableRowData.get("label") + "] \n [" + tableRowData.get("label") + "]"
                    }
                    break;
                default :
                    if (index === 0) {
                        textForUMLDiagram += "@startuml \n" + tableRowData.get("queue_name")
                    } else {
                        textForUMLDiagram += " --> " +  tableRowData.get("queue_name") + ": " + tableRowData.get("label") + " \n " + tableRowData.get("queue_name")
                    }
                    break;

            }
        });
        return textForUMLDiagram;
    }

    render() {
        "use strict";
        let encodedStringForPlantUML = this._getUMLDiagramDataString();
        if( this.state.selectedTab !== 1 ) {
            encodedStringForPlantUML += "\n @enduml";
        }
        const encoded = plantumlEncoder.encode(encodedStringForPlantUML);
        return (
            <div className="diagram-tab__container">
                <Tabs activeKey={this.state.selectedTab} onSelect={this.handleSelect}>
                    <Tab eventKey={1} title="Component diagram">
                        <div>
                            <div className="diagram__container">
                                {encodedStringForPlantUML !== "" ? <img className="diagram" src={'http://www.plantuml.com/plantuml/img/' + encoded}></img> : null}
                            </div>
                        </div>
                     </Tab>
                    <Tab eventKey={2} title="Sequence diagram">
                        <div>
                            <div className="diagram__container">
                                {encodedStringForPlantUML !== "" ? <img className="diagram" src={'http://www.plantuml.com/plantuml/img/' + encoded}></img> : null}
                            </div>
                        </div>
                     </Tab>
                    <Tab eventKey={3} title="Diagram text">
                        <ReactQuill value={encodedStringForPlantUML}>
                        </ReactQuill>
                    </Tab>
                </Tabs>
            </div>
        );
    }

}

export default Diagram;
