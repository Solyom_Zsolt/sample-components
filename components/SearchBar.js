/*! React Starter Kit | MIT License | http://www.reactstarterkit.com/ */

import React, { PropTypes, Component } from 'react';
import Immutable from 'immutable';

class SearchBar extends Component {

    constructor(props) {
        "use strict";
        super(props);
    }

    render() {
        "use strict";
        return (
            <div className="search-bar__container">
                <span className="search-bar__description-text">Filter
                </span>
                <input className="search-bar__input" placeholder="Search" onChange={this.props.onSearchInputChange}>
                </input>
            </div>
        );
    }

}

export default SearchBar;
