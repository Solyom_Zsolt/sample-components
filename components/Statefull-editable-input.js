/*! React Starter Kit | MIT License | http://www.reactstarterkit.com/ */

import React, { PropTypes, Component } from 'react';
import Immutable from 'immutable';

import classNames from 'classnames';

import checkmark from '../../img/checkmark.png';
import xmark from '../../img/xmark.png';

class StatefullEditableInput extends Component {

    constructor(props) {
        "use strict";
        super(props);
        this.state = {
            "editable": false
        };
        this._onInputClick = this._onInputClick.bind(this);
        this._onEditorClick = this._onEditorClick.bind(this);
        this._onKeyPressed = this._onKeyPressed.bind(this);
        this.onCheckMarkClick = this.onCheckMarkClick.bind(this);
        this.onXMarkClick = this.onXMarkClick.bind(this);
    }

    _onInputClick() {
        "use strict";
        this.setState({
            "editable": true
        });
    }

    onCheckMarkClick(e) {
        "use strict";
        e.stopPropagation();
        this.setState({
            "editable": false
        });
        this.props.onSetRowLabel(this.props.rowData, this.refs.editor.value);
    }

    onXMarkClick(e) {
        "use strict";
        e.stopPropagation();
        this.setState({
            "editable": false
        });
    }

    _onEditorClick(e) {
        "use strict";
        e.stopPropagation();
    }

    _onKeyPressed(e) {
        "use strict";
        switch (e.keyCode) {
            case 13:

                this.setState({
                    "editable": false
                });
                this.props.onSetRowLabel(this.props.rowData, this.refs.editor.value);

                break;
            case 27:

                this.setState({
                    "editable": false
                });

                break;
            default:

        }
    }

    render() {
        "use strict";

        var inputContainerClasses = classNames({
            "editable-input__container": true,
            "--selected": this.state.editable
        });

        return (
            <div className={inputContainerClasses}
                onClick={this._onInputClick}>
                {
                    this.state.editable ?
                    <div>
                        <input autoFocus ref="editor"
                                className="editable-input__editor"
                                onClick={this._onEditorClick}
                                onKeyDown={this._onKeyPressed}>
                        </input>
                        <img src={checkmark}
                            className="editable-input__checkmark"
                            onClick={this.onCheckMarkClick}/>
                        <img src={xmark}
                            className="editable-input__xmark"
                            onClick={this.onXMarkClick}/>
                    </div>:
                    <div>
                        {this.props.rowData.get("label")}
                    </div>
                }
            </div>
        );
    }

}

export default StatefullEditableInput;
